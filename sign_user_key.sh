#!/bin/bash

set -eu

readonly BASE_DIR="$(dirname "$0")"
readonly KEY_DIR="${BASE_DIR}/keys"
readonly CERT_DIR="${BASE_DIR}/certs"
readonly VARIABLES="${BASE_DIR}/variables"

if [ $# -ne 2 ]
then
    echo "Usage: $(basename "$0") <pubkey-to-sign> <key-id>"
    echo
    echo "<key-id> should be something simple like \"peters_default_key\""
    exit 1
fi

if [ ! -f "${VARIABLES}" ]
then
    echo "Could not load ${VARIABLES} file." >&2
    exit 1
fi
source "${VARIABLES}"

readonly KEY_FILE="$1"
readonly KEY_ID="$2"
readonly SERIAL="$(date +%s)"

ssh-keygen -s "${KEY_DIR}/ca_key" -I "$KEY_ID" -n "${PRINCIPAL}" -V "${VALIDITY}" -z "$SERIAL" "$KEY_FILE"

readonly CERT_FILE="${KEY_FILE%.pub}-cert.pub"

if [ ! -f "${CERT_FILE}" ]
then
    echo "Could not find certificate file \"${CERT_FILE}\"" >&2
    echo "It will not be archived in ${CERT_DIR}" >&2
    exit 1
fi

readonly ARCHIVED_CERT_FILE="${SERIAL}_$(basename "${KEY_FILE}")"
cp "${CERT_FILE}" "${CERT_DIR}/${ARCHIVED_CERT_FILE}"
echo "The new certificate has been archived in ${CERT_DIR} as ${ARCHIVED_CERT_FILE}"
echo "You should commit these changes to the repository for future reference."

